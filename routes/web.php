<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $about = array(
		"Server Name : " => "Infinity Infotech Ltd. - Orders Management Module API Service",
		"Version : " => "1.0.0",
		"Published : " => "22 February, 2022",
		"Author : " => "Fahim Uddin",
		"Framework : " =>  $router->app->version(),
		"Connected Database" => (app('db')->connection()->getDatabaseName()) ? app('db')->connection()->getDatabaseName() : "No DB Selected"
	);
	return json_encode($about, JSON_PRETTY_PRINT);
});

/** 
 * API Version 1
 * ============================================================================================================
**/

$router->group(['prefix' => 'api/v1'], function() use ($router) {

    $router->group(['prefix' => 'admin'], function() use ($router) {
    
        $router->group(['prefix' => 'ecommerce'], function() use ($router) {
    
            // Orders
            $router->get('/orders',  ['uses' => 'Admin\Ecommerce\OrderController@index']);
            $router->get('/orders/{id}',  ['uses' => 'Admin\Ecommerce\OrderController@show']);
            $router->get('/orders/status/{status_slug}', ['uses' => 'Admin\Ecommerce\OrderController@index_by_status']);

            $router->get('/orders/{type}/{object_id}/totals',  ['uses' => 'Admin\Ecommerce\OrderController@show_totals']);

            // Invoices
            $router->get('/invoices/{type}/{object_id}',  ['uses' => 'Admin\Ecommerce\OrderController@show_invoice']);
            

            // Packages
            $router->get('/orders/{id}/packages',  ['uses' => 'Admin\Ecommerce\PackageController@index_by_order']);
            $router->get('/orders/{order_id}/packages/{package_id}/items',  ['uses' => 'Admin\Ecommerce\PackageController@get_package_items_by_package_id']);
            
            // Statuses
            $router->get('/statuses',  ['uses' => 'Admin\Ecommerce\StatusController@index']);
            $router->get('/statuses/{slug}',  ['uses' => 'Admin\Ecommerce\StatusController@show_by_slug']);
            $router->get('/statuses/relations/{type}/{id}',  ['uses' => 'Admin\Ecommerce\StatusController@relations']);

            $router->post('/statuses/relations/', ['uses' => 'Admin\Ecommerce\StatusController@update_status']);
            



            // Others Routes Only For Now
            $router->get('/customers/{id}',  ['uses' => 'Admin\Ecommerce\OtherController@getCustomerByID']);
            $router->get('/sellers/{id}',  ['uses' => 'Admin\Ecommerce\OtherController@getSellerByID']);
            
        });
        
    });
});




