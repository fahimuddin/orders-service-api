<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;

class CorsMiddleware {

    public function handle($request, \Closure $next)
    {
        $headers = [
            'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'GET, POST, PUT, PATCH, OPTIONS, DELETE',
            //'Access-Control-Allow-Methods'     => '*',
            //'Access-Control-Allow-Methods'     => '*',
            //'Access-Control-Allow-Credentials' => 'true',
            //'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => 'Accept, Content-Type, Origin, Authorization, X-Requested-With, Content-Language, Subject'
            //'Access-Control-Allow-Headers'     => '*'
        ];
        
        if ($request->isMethod('OPTIONS'))
        {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
        {
            $response->header($key, $value);
        }

        
    //   Log::info('-------------------------------------------------------------');
    //   Log::info($request);
    //   Log::info('-------------------------------------------------------------');
    //   //Log::info($response);
    //   //Log::info('-------------------------------------------------------------');

        return $response;
    }

}
