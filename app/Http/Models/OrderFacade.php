<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class OrderFacade
{
    public static function filter($value, $type)
    {
        if($type == 'status') {
            try {
                $columns = [
                    'o.id',
                    'o.order_no',
                    'o.payment_status_id',
                    'o.fulfillment_status_id',
                    'o.payment_method_id',
                    'o.customer_id',
                    'c.ecom_customer_user_full_name AS customer_name',
                    'o.created_at',
                    'o.updated_at',

                    'st.id AS status_id',
                    'st.status_name',
                    'st.status_color',
                    'st.status_type'
                ];
                
                $result = DB::table(config('tables.Orders').' AS o')
                            ->join(config('tables.Customers').' AS c', 'o.customer_id', '=', 'c.id')
                            ->join(config('tables.StatusRelations').' AS sr', 'o.id', '=', 'sr.object_id')
                            ->join(config('tables.Statuses').' AS st', 'sr.status_id', '=', 'st.id')
                            ->select($columns)
                            ->where('sr.object_type', 'order')
                            ->where('st.status_slug', $value)
                            ->whereNull('o.deleted_at')
                            ->latest('o.created_at')
                            ->get();
    
                return $result;
            } catch (MySqlException $e) {
                return response()->json($e, 200);
            }
        }
        
    }
}

