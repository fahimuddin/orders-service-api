<?php

namespace App\Http\Controllers\Admin\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Support\Facades\Log;

use Carbon;
// use App\Helper;

class OtherController extends Controller 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Response
     */
    private $response = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->response = [
          'message' => 'Server Error Occured',
          'status' => 'Internal Server Error',
          'code' => 500,
          'data' => null
        ];

    }

    public function getCustomerByID($id) {
        try {
            $columns = [
                'c.id',
                'c.ecom_customer_id AS customer_code',
                'c.ecom_customer_user_full_name AS customer_name',
                'c.ecom_customer_user_number AS customer_phone',
                'c.ecom_customer_user_email AS customer_email',
                'c.status AS customer_status'
            ];

            $customer = DB::table(config('tables.Customers').' AS c')
                        ->whereNull('c.deleted_at')
                        ->where('c.id', $id)
                        ->select($columns)
                        ->latest('c.create_at')
                        ->first();

            $this->response = [
                'message' => $customer ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $customer ? 'OK' : 'Not Found',
                'code' => $customer ? 200 : 404,
                'data' => $customer ? $customer : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function getSellerByID($id) {
        try {
            $columns = [
                's.id',
                's.shop_name',
                's.shop_logo',
                's.shop_thumbnail',
                's.shop_banner',
                's.seller_name',
                's.seller_type',
                's.account_type',
                's.email',
                's.phone_number',
                's.alt_mobile',
                's.status',
                's.entrydate',
                's.user_id'
            ];

            $item = DB::table(config('tables.Sellers').' AS s')
                        ->where('s.user_id', $id)
                        ->select($columns)
                        ->first();

            $this->response = [
                'message' => $item ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $item ? 'OK' : 'Not Found',
                'code' => $item ? 200 : 404,
                'data' => $item ? $item : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

}