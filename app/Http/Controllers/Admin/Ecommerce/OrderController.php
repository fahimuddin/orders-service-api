<?php

namespace App\Http\Controllers\Admin\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Support\Facades\Log;

use Carbon;
// use App\Helper;

// Models
use App\Http\Models\OrderFacade;


class OrderController extends Controller 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Response
     */
    private $response = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->response = [
          'message' => 'Server Error Occured',
          'status' => 'Internal Server Error',
          'code' => 500,
          'data' => null
        ];

    }

    public function index()
    {
        $value = null;
        if($this->request->has('value'))
        {
            // Decode The Value
            try {
                $value = urldecode($this->request->value);
                return response()->json($value, 200);
            } catch (Exception $e) {
                return response()->json($e, 200);
            }
        }

        try {
            $columns = [
                'o.id',
                'o.order_no',
                'o.payment_status_id',
                'o.fulfillment_status_id',
                'o.payment_method_id',
                'o.customer_id',
                'c.ecom_customer_user_full_name AS customer_name',
                'o.created_at',
                'o.updated_at',

                // 'st.id',
                // 'st.status_name',
                // 'st.status_color',
                // 'st.status_type'
            ];

            $orders = DB::table(config('tables.Orders').' AS o')
                            ->join(config('tables.Customers').' AS c', 'o.customer_id', '=', 'c.id')
                            //->leftJoin(config('tables.StatusRelations').' AS sr', 'o.id', '=', 'sr.object_id')
                            //->leftJoin(config('tables.Statuses').' AS st', 'sr.status_id', '=', 'st.id')
                            ->select($columns)
                            //->where('sr.object_type', 'order')
                            ->whereNull('o.deleted_at')
                            ->latest('o.created_at')
                            ->get();

            // $orders = DB::table(config('db.tables.EcomOrders').' AS o')
            //             ->join(config('db.tables.SiteUserDetails').' AS ud', 'ud.user_id', '=', 'o.user_id')
            //             ->join(config('db.tables.EcomOrderStatuses').' AS os', 'os.order_status_id', '=', 'o.order_status_id')
            //             ->join(config('db.tables.EcomOrderStatusDetails').' AS osd', 'osd.order_status_id', '=', 'os.order_status_id')
            //             ->where('osd.order_status_details_language_id', 1)
            //             ->select($columns)
            //             ->latest('o.order_created_at')
            //             ->get();

            $this->response = [
                'message' => $orders ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $orders ? 'OK' : 'Not Found',
                'code' => $orders ? 200 : 404,
                'data' => $orders ? $orders : NULL
            ];

            // $this->response = [
            //     'message' => 'Successfully Found !',
            //     'status' => 'OK',
            //     'code' => 200,
            //     'data' => null
            // ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function index_by_status($slug) {
        try {
            $resource = OrderFacade::filter($slug, 'status');

            $this->response = [
                'message' => $resource ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $resource ? 'OK' : 'Not Found',
                'code' => $resource ? 200 : 404,
                'data' => $resource ? $resource : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function show($id) {
        try {
            $columns = [
                'o.id',
                'o.order_no',
                'o.payment_status_id',
                'o.fulfillment_status_id',
                'o.payment_method_id',
                'o.customer_id',
                'o.created_at',
                'o.updated_at'
            ];

            $order = DB::table(config('tables.Orders').' AS o')
                        // ->join(config('tables.Customers').' AS c', 'o.customer_id', '=', 'c.id')
                        ->whereNull('o.deleted_at')
                        ->where('o.id', $id)
                        ->select($columns)
                        ->first();

            $this->response = [
                'message' => $order ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $order ? 'OK' : 'Not Found',
                'code' => $order ? 200 : 404,
                'data' => $order ? $order : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
                
    }

    public function show_totals($object_id, $type) {
        //return response()->json([$object_id, $type], 200);
        try {
            $columns = [
                'ot.id',
                'ot.total_type',
                'ot.total_code',
                'ot.total_title',
                'ot.total_value',
                'ot.total_sort_order'
            ];

            $items = DB::table(config('tables.OrderTotals').' AS ot')
                        ->select($columns)
                        ->where('ot.total_type', $type)
                        ->where('ot.object_id', $object_id)
                        ->orderBy('ot.total_sort_order', 'ASC')
                        ->get();
                        //->toSql();
            //return response()->json($items, 200);
            $this->response = [
                'message' => $items ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $items ? 'OK' : 'Not Found',
                'code' => $items ? 200 : 404,
                'data' => $items ? $items : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function show_invoice($object_id, $type) {
        try {
            

            $tableName = '';
            $items = [];

            if($type == 'package') {
                $columns = [
                    'inv.id',
                    'inv.prefix',
                    'inv.code',
                    'inv.postfix',
                    'inv.object_type',
                    'inv.object_id',
                    'inv.object_no',
                    'inv.created_at'
                ];

                $tableName = config('tables.Packages').' AS tn';

                $items = DB::table(config('tables.Invoices').' AS inv')
                        ->join($tableName, 'inv.object_id', '=', 'tn.id')
                        ->select($columns)
                        ->where('inv.object_type', $type)
                        ->where('inv.object_id', $object_id)
                        ->get();

                foreach ($items as $package) {
                    $columns = [
                        'pi.id',
                        'pi.item_id',
                        'pi.static_item_name',
                        'pi.static_item_quantity',
                        'pi.static_item_price',
                        'pi.static_item_special_price',
                        'pi.static_item_sub_total'
                    ];

                    $package->items = DB::table(config('tables.PackageItems').' AS pi')
                                                ->select($columns)
                                                ->where('pi.package_id', $package->object_id)
                                                ->get();

                    $package->totals = $this->show_totals($package->id, 'package')->original['data'];
                }
                
            }

            if($type == 'order') {

                $columns = [
                    'inv.id',
                    'inv.prefix',
                    'inv.code',
                    'inv.postfix',
                    'inv.object_type',
                    'inv.object_id',
                    'inv.object_no',
                    'inv.created_at',
    
                    'tn.customer_id',
                    'c.ecom_customer_user_full_name AS customer_name',
                    'c.ecom_customer_user_email AS customer_email',
                    'c.ecom_customer_user_number AS customer_phone',
                ];

                $tableName = config('tables.Orders').' AS tn';

                $items = DB::table(config('tables.Invoices').' AS inv')
                        ->join($tableName, 'inv.object_id', '=', 'tn.id')
                        ->leftJoin(config('tables.Customers').' AS c', 'tn.customer_id', '=', 'c.id')
                        ->select($columns)
                        ->where('inv.object_type', $type)
                        ->where('inv.object_id', $object_id)
                        ->first();

                $columns = [
                    'p.id',
                    'p.package_no',
                    'p.seller_id',
                    'p.order_id',
                    'p.created_at',

                    's.shop_name',
                    's.seller_name'
                ];

                $items->packages = DB::table(config('tables.Packages').' AS p')
                                    ->leftJoin(config('tables.Sellers').' AS s', 's.user_id', '=', 'p.seller_id')
                                    ->select($columns)
                                    ->where('p.order_id', $items->object_id)
                                    ->get();

                foreach ($items->packages as $package) {
                    // $order->order_created_at = Carbon\Carbon::parse($order->order_created_at)->diffForHumans();
                    $columns = [
                        'pi.id',
                        'pi.item_id',
                        'pi.static_item_name',
                        'pi.static_item_quantity',
                        'pi.static_item_price',
                        'pi.static_item_special_price',
                        'pi.static_item_sub_total'
                    ];

                    $package->items = DB::table(config('tables.PackageItems').' AS pi')
                                                ->select($columns)
                                                ->where('pi.package_id', $package->id)
                                                ->get();

                    $package->totals = $this->show_totals($package->id, 'package')->original['data'];
                }
                    
            }

            $this->response = [
                'message' => $items ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $items ? 'OK' : 'Not Found',
                'code' => $items ? 200 : 404,
                'data' => $items ? $items : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function create_invoice() {

    }

    public function insert() {
        // Create a new order







        $inputs = [];

        $inputs[$count]['order_total_type'] = 'package';

        $inputs[$count]['order_total_code'] = 'sub_total';
        $inputs[$count]['order_total_title'] = 'Sub-Total';
        $inputs[$count]['order_total_value'] = $sub_total;
        $inputs[$count]['order_total_sort_order'] = 1;
        $inputs[$count]['order_id'] = $entry_id;

        // $inputs[$count]['object_id'] = $entry_id;

        $count++;

        $inputs[$count]['order_total_code'] = 'shipping';
        $inputs[$count]['order_total_title'] = 'Flat Shipping Rate';
        $inputs[$count]['order_total_value'] = $shipping;
        $inputs[$count]['order_total_sort_order'] = 3;
        $inputs[$count]['order_id'] = $entry_id;

        $count++;

        // Preparing Inputs
        $inputs[$count]['order_total_code'] = 'coupon';
        $inputs[$count]['order_total_title'] = 'Coupon ('. $this->request->coupons[$i]['coupon_code'] .')';
        $inputs[$count]['order_total_value'] = -($amount);
        $inputs[$count]['order_total_sort_order'] = 4 + $i;
        $inputs[$count]['order_id'] = $entry_id;

        $count++;

        $inputs[$count]['order_total_code'] = 'total';
        $inputs[$count]['order_total_title'] = 'Total';
        $inputs[$count]['order_total_value'] = $total;
        $inputs[$count]['order_total_sort_order'] = 100;
        $inputs[$count]['order_id'] = $entry_id;


        $success = DB::table(config('db.tables.EcomOrderTotals'))->insert( $inputs );


        $columns_totals = [
            'ot.order_total_title',
            'ot.order_total_value'
        ];

        $order->order_totals = DB::table(config('db.tables.EcomOrderTotals').' AS ot')
                                    ->where('ot.order_id', $order->order_id)
                                    ->select($columns_totals)
                                    ->orderBy('ot.order_total_sort_order')
                                    ->get();


        // <tr *ngFor="let total of order.order_totals; let i = index">
        //     <td class="text-right" colspan="4"><strong>{{ total.order_total_title }}</strong></td>
        //     <td class="text-right"><strong>৳ {{ total.order_total_value }}</strong></td>
        // </tr>
    }


}