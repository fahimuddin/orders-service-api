<?php

namespace App\Http\Controllers\Admin\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Support\Facades\Log;

use Carbon;
// use App\Helper;

class StatusController extends Controller 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Response
     */
    private $response = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->response = [
          'message' => 'Server Error Occured',
          'status' => 'Internal Server Error',
          'code' => 500,
          'data' => null
        ];

    }

    public function index() {
        try {
            $columns = [
                'st.id',
                'st.status_type', // Payment or Order
                'st.status_code',
                'st.status_slug',
                'st.status_name',
                'st.status_description',
                'st.status_comment',
                'st.icon_type',
                'st.icon_name',
                'st.icon_url',
                'st.status_color',
                'st.is_active',
                'st.created_at',
                'st.created_by',
                'st.updated_at',
                'st.updated_by',
                'st.deleted_at',
                'st.deleted_by'
            ];

            $items = DB::table(config('tables.Statuses').' AS st')
                        ->whereNull('st.deleted_at')
                        ->select($columns)
                        ->latest('st.created_at')
                        ->get();

            $this->response = [
                'message' => $items ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $items ? 'OK' : 'Not Found',
                'code' => $items ? 200 : 404,
                'data' => $items ? $items : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function show_by_slug($slug) {
        try {
            $columns = [
                'st.id',
                'st.status_type', // Payment or Order
                'st.status_code',
                'st.status_slug',
                'st.status_name',
                'st.status_description',
                'st.status_comment',
                'st.icon_type',
                'st.icon_name',
                'st.icon_url',
                'st.status_color',
                'st.is_active',
                'st.created_at',
                'st.created_by',
                'st.updated_at',
                'st.updated_by',
                'st.deleted_at',
                'st.deleted_by'
            ];

            $items = DB::table(config('tables.Statuses').' AS st')
                        ->whereNull('st.deleted_at')
                        ->where('st.is_active', 1)
                        ->where('st.status_slug', $slug)
                        ->select($columns)
                        ->first();

            $this->response = [
                'message' => $items ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $items ? 'OK' : 'Not Found',
                'code' => $items ? 200 : 404,
                'data' => $items ? $items : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function relations($id, $type) {
        try {
            $columns = [
                'sr.id',
                'sr.status_type',
                'sr.status_id',
                'sr.created_at',

                's.status_name',
                's.status_color'
            ];

            $resource = DB::table(config('tables.StatusRelations').' AS sr')
                        ->join(config('tables.Statuses').' AS s', 'sr.status_id', '=', 's.id')
                        ->where('sr.object_type', $type)
                        ->where('sr.object_id', $id)
                        ->select($columns)
                        ->get();

            $this->response = [
                'message' => $resource ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $resource ? 'OK' : 'Not Found',
                'code' => $resource ? 200 : 404,
                'data' => $resource ? $resource : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function update_status() {
        try {
            $object_type = $this->request->object_type;
            $object_id = $this->request->object_id;
            $status_type = $this->request->status_type;
            $status_id = $this->request->status_id;


            $updated = DB::table(config('tables.StatusRelations'))
                        ->where('object_type', $object_type)
                        ->where('object_id', $object_id)
                        ->where('status_type', $status_type)
                        ->update([
                            'status_id' => $status_id
                        ]);

            $this->response = [
                'message' => $updated ? 'Successfully Updated !' : 'Nothing to Update !',
                'status' => $updated ? 'OK' : 'Not Processed',
                'code' => $updated ? 200 : 200,
                'data' => $updated ? $updated : 0
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }
}