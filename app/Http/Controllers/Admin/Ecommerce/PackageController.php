<?php

namespace App\Http\Controllers\Admin\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Support\Facades\Log;

use Carbon;
// use App\Helper;

class PackageController extends Controller 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Response
     */
    private $response = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->response = [
          'message' => 'Server Error Occured',
          'status' => 'Internal Server Error',
          'code' => 500,
          'data' => null
        ];

    }

    // Get All Packages Under Seller By Seller ID
    public function index_by_seller($seller_id) {

    }

    // Get All Packages Under Order By Order ID
    public function index_by_order($order_id) {
        try {
            $columns = [
                'p.id',
                'p.package_no',
                'p.package_status_id',
                'p.package_type_id',
                'p.seller_id',
                'p.order_id',
                'p.created_at',
                'p.updated_at'
            ];

            $items = DB::table(config('tables.Packages').' AS p')
                        ->whereNull('p.deleted_at')
                        ->where('p.order_id', $order_id)
                        ->select($columns)
                        ->latest('p.created_at')
                        ->get();

            $this->response = [
                'message' => $items ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $items ? 'OK' : 'Not Found',
                'code' => $items ? 200 : 404,
                'data' => $items ? $items : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    public function get_package_items_by_package_id($package_id) {
        try {
            $columns = [
                'i.id',
                'i.item_id',
                'i.seller_id',
                'i.package_id',
                'i.static_item_name',
                'i.static_item_quantity',
                'i.static_item_price',
                'i.static_item_special_price',
                'i.static_item_sub_total',
                'i.static_item_type_id',
                'i.created_at',
            ];

            $items = DB::table(config('tables.PackageItems').' AS i')
                        ->where('i.package_id', $package_id)
                        ->select($columns)
                        ->latest('i.created_at')
                        ->get();

            $this->response = [
                'message' => $items ? 'Successfully Found !' : 'Failed to find requested content !',
                'status' => $items ? 'OK' : 'Not Found',
                'code' => $items ? 200 : 404,
                'data' => $items ? $items : NULL
            ];

            return response()->json($this->response, $this->response['code']);
        } catch (MySqlException $e) {
            return response()->json($e, 404);
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }
}