<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Database Configuration - Table Names
    |--------------------------------------------------------------------------
    |
    */

    // Orders
    'Orders' => 'ecom_orders',
    'Packages' => 'ecom_packages',
    'PackageItems' => 'ecom_package_items',
    'OrderTotals' => 'ecom_order_totals',

    // Invoices
    'Invoices' => 'ecom_order_invoices',

    // Statuses
    'Statuses' => 'ecom_statuses',
    'StatusRelations' => 'ecom_status_relations',


    // Products
    'Products' => 'ecom_products',
    'ProductDetails' => 'ecom_product_details',
    'ProductsGallery' => 'ecom_products_gallery',

    // OTHERS ----------------------------------------------
    // Customer
    'Customers' => 'ecom_customer',
    // Seller
    'Sellers' => 'ecom_seller'

];
